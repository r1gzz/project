$(function () {
    $(".header__inner nav ul li.switch a").click(function (e) {
        $("body").toggleClass("switchMode");
        e.preventDefault();
    })
})

var element = document.getElementById('modal-window');

var t = setTimeout(openModalWindow, 1500);
function openModalWindow() {
    document.getElementById('modal-window');
    element.style.visibility = "visible";
    element.style.opacity = "1";
    t & window.clearTimeout(t);
}

$('.navigation-bar-list__item').click(function () {
    $('.navigation-bar .navigation-bar__list').toggleClass("active");
    $('.navigation-bar-list__item i').toggleClass("active");
});
